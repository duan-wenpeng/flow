/*
 * @Descripttion:
 * @version:
 * @Author: duanwenpeng
 * @Date: 2022-09-23 14:56:06
 * @LastEditors: duanwenpeng
 * @LastEditTime: 2022-11-21 10:42:49
 */
import Vue from "vue";
import flow from "../src/App.vue";
const install = function (Vue) {
    Vue.component(flow.name, flow);
};
//  全局引用可自动安装
if (typeof window !== "undefined" && window.Vue) {
    install(window.Vue);
}

export default {
    install,
};
