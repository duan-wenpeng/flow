import JsSIP from "jssip";

export const sipInit = () => {
    let outgoingSession = null;
    let incomingSession = null;
    let currentSession = null;
    let localStream = null;
    let userAgent = null;

    JsSIP.C.SESSION_EXPIRES = 120;
    JsSIP.C.MIN_SESSION_EXPIRES = 120;

    function testStart(ext, registerCallbacks) {
        // 分机号注册 格式 sip: + 分机号码 + @ + FS注册地址
        let sip_uri_ = "sip:" + ext + "@192.168.5.84:5060";
        // FS密码
        let sip_password_ = "1234";
        // Fs的 ws协议地址
        let ws_uri_ = "ws://192.168.5.84:5066/";

        const { registrationFailed, registered, registrationExpiring } = registerCallbacks;

        console.info("get input info: sip_uri = ", sip_uri_, " sip_password = ", sip_password_, " ws_uri = ", ws_uri_);

        let socket = new JsSIP.WebSocketInterface(ws_uri_);

        let configuration = {
            sockets: [socket],
            outbound_proxy_set: ws_uri_,
            uri: sip_uri_,
            password: sip_password_,
            register: true,
            session_timers: false,
        };

        userAgent = new JsSIP.UA(configuration);

        //注册成功
        userAgent.on("registered", registered);

        //注册失败
        userAgent.on("registrationFailed", registrationFailed);

        //注册超时
        userAgent.on("registrationExpiring", registrationExpiring);

        userAgent.on("newRTCSession", function (data) {
            //通话呼入
            if (data.originator == "remote") {
                // 接电话
                incomingSession = data.session;
                data.session.answer({
                    mediaConstraints: {
                        audio: true,
                        video: false,
                        mandatory: { maxWidth: 640, maxHeight: 360 },
                    },
                    mediaStream: localStream,
                });
            } else {
                // 打电话
                outgoingSession = data.session;
                outgoingSession.on("connecting", function () {
                    currentSession = outgoingSession;
                    outgoingSession = null;
                });
            }

            data.session.on("accepted", function (data) {
                console.info("onAccepted - ", data);
                if (data.originator == "remote" && currentSession == null) {
                    currentSession = incomingSession;
                    incomingSession = null;
                    console.info("setCurrentSession - ", currentSession);
                }
            });
            data.session.on("confirmed", function (data) {
                console.info("onConfirmed - ", data);
                if (data.originator == "remote" && currentSession == null) {
                    currentSession = incomingSession;
                    incomingSession = null;
                    console.info("setCurrentSession - ", currentSession);
                }
            });
            data.session.on("sdp", function (data) {
                console.info("onSDP, type - ", data.type, " sdp - ", data.sdp);
            });

            data.session.on("progress", function (data) {
                console.info("onProgress - ", data.originator);
                if (data.originator == "remote") {
                    console.info("onProgress, response - ", data.response);
                }
            });
            data.session.on("peerconnection", function (data) {
                console.info("onPeerconnection - ", data.peerconnection);
                data.peerconnection.onaddstream = function (ev) {
                    console.info("onaddstream from remote ----------- ", ev);
                    // 需要连接页面的 vidio 标签
                    let videoView = document.getElementById("localVideo");
                    videoView.srcObject = ev.stream;
                };
            });
        });

        userAgent.on("newMessage", function (data) {
            if (data.originator == "local") {
                console.info("onNewMessage , OutgoingRequest - ", data.request);
            } else {
                console.info("onNewMessage , IncomingRequest - ", data.request);
            }
        });

        console.info("call register");

        userAgent.start();
    }

    // 拨号
    function testCall(sip_phone_number_, ended) {
        // Register callbacks to desired call events
        var eventHandlers = {
            progress: function (e) {
                console.log("call is in progress");
            },
            failed: function (e) {
                console.log("call failed: ", e);
            },
            ended,
            confirmed: function (e) {
                console.log("call confirmed", e);
            },
        };

        let options = {
            eventHandlers: eventHandlers,
            mediaConstraints: {
                audio: true,
                video: false,
                mandatory: { maxWidth: 640, maxHeight: 360 },
            },
        };
        outgoingSession = userAgent.call(sip_phone_number_, options);
    }

    // 注册
    function reg() {
        console.log("register----------->");
        userAgent.register();
    }

    // 取消注册
    function unReg() {
        console.log("unregister----------->");
        userAgent.unregister(true);
    }

    // 挂断
    function hangup() {
        console.log("hangup----------->");
        userAgent.terminateSessions();
    }

    return {
        testStart,
        testCall,
        reg,
        unReg,
        hangup,
    };
};
