/*
 * @Descripttion:
 * @version:
 * @Author: duanwenpeng
 * @Date: 2022-11-21 14:37:08
 * @LastEditors: duanwenpeng
 * @LastEditTime: 2023-03-16 14:42:50
 */
export default function () {
    return {
        data() {
            return {
                sort: 0,
            };
        },
        methods: {
            addSort(data, nodeId) {
                data.children.forEach((t, i) => {
                    if (nodeId) {
                        t.parentId = nodeId;
                    }
                    t.sort = i + 1;
                    if (t.children && t.children.length && t.children.length > 0) {
                        this.addSort(t, t.id);
                    }
                });
                return data;
            },
        },
    };
}
