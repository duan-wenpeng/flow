/*
 * @Descripttion: 
 * @version: 
 * @Author: duanwenpeng
 * @Date: 2022-10-14 13:36:34
 * @LastEditors: duanwenpeng
 * @LastEditTime: 2022-10-17 09:50:05
 */
import Vue from 'vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import App from './App.vue';

Vue.use(ElementUI);

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
